/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/29 22:00:24 by samzur            #+#    #+#             */
/*   Updated: 2020/09/29 22:50:38 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(const char *str, ...)
{
	va_list	args;
	int		size;
	int		len;

	len = 0;
	va_start(args, str);
	while (*str)
	{
		while (*str && *str != '%')
		{
			ft_putchar(*str);
			len++;
			str++;
		}
		if (*str == '%')
		{
			if ((size = parser(str, &args, &len)) == -1)
				return (-1);
			if (size == 0)
				ft_putchar(*str);
			str += size + 1;
		}
	}
	va_end(args);
	return (len);
}
