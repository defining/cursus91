/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 20:28:31 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 21:00:19 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char *source;
	unsigned char *dest;

	dest = (unsigned char *)dst;
	source = (unsigned char *)src;
	while (n-- > 0)
	{
		*dest = *source;
		dest++;
		if (*source == (unsigned char)c)
			return ((void *)dest);
		source++;
	}
	return (NULL);
}
