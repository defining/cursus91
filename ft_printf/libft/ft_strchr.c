/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 01:59:08 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 02:33:08 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char *src;

	src = (char *)s;
	while (*src)
	{
		if (*src == c)
		{
			return (src);
		}
		else
			src++;
	}
	if (*src == c)
	{
		return (src);
	}
	return (NULL);
}
