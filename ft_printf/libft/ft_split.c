/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/08 03:06:40 by samzur            #+#    #+#             */
/*   Updated: 2020/07/09 04:02:31 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_copy_next_split(int *i, char const *str, char sep)
{
	int		j;
	char	*next_str;

	j = 0;
	while (str[*i + j] && str[*i + j] != sep)
		j++;
	if (!(next_str = malloc(sizeof(char) * (j + 1))))
		return (NULL);
	j = 0;
	while (str[*i + j] && str[*i + j] != sep)
	{
		next_str[j] = str[*i + j];
		j++;
	}
	next_str[j] = '\0';
	*i += j;
	return (next_str);
}

char	**ft_split(char const *str, char sep)
{
	char	**tab;
	int		i;
	int		j;

	if (str == NULL)
		return (0);
	i = ft_count_split(str, sep);
	if (!(tab = malloc(sizeof(char *) * (i + 1))))
		return (NULL);
	i = 0;
	j = 0;
	while (str[i])
	{
		if (!(tab[j] = ft_copy_next_split(&i, str, sep)))
			return (ft_free_tab_str(j, tab));
		else if (tab[j][0] != '\0')
			j++;
		else
		{
			free(tab[j]);
			i++;
		}
	}
	tab[j] = NULL;
	return (tab);
}
