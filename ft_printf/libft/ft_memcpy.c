/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 19:02:07 by samzur            #+#    #+#             */
/*   Updated: 2020/07/09 04:09:48 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	int				i;
	unsigned char	*dest;
	unsigned char	*srcs;

	if (dst == src)
		return (dst);
	i = 0;
	dest = (unsigned char *)dst;
	srcs = (unsigned char *)src;
	while (n-- > 0)
	{
		dest[i] = srcs[i];
		i++;
	}
	return (dst);
}
