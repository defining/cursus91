/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_split_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 01:55:38 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 01:55:42 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_count_split(const char *str, char c)
{
	int	i;
	int	nb;

	i = -1;
	nb = 0;
	while (str[++i])
	{
		if (i == 0 && str[i] != c)
			nb++;
		else if (str[i] != c && str[i - 1] == c)
			nb++;
	}
	return (nb);
}
