/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse_str_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 01:56:45 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 01:56:48 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_reverse_str(char *src)
{
	int		i;
	int		j;
	char	tmp;

	i = 0;
	while (src[i])
		i++;
	if (i == 1)
		return (src);
	i--;
	j = 0;
	while (j <= i / 2)
	{
		tmp = src[i - j];
		src[i - j] = src[j];
		src[j] = tmp;
		j++;
	}
	return (src);
}
