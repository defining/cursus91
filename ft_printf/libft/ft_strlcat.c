/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 15:43:38 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 16:00:12 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	int				i;
	unsigned int	rdstsize;
	unsigned int	rsrcsize;

	i = 0;
	rdstsize = 0;
	rsrcsize = 0;
	while (dst[rdstsize] != '\0')
		rdstsize++;
	while (src[rsrcsize] != '\0')
		rsrcsize++;
	if (dstsize <= rdstsize)
		return (rsrcsize + dstsize);
	rsrcsize += rdstsize;
	while (rdstsize < (dstsize - 1) && src[i] != '\0')
	{
		dst[rdstsize] = src[i++];
		rdstsize++;
	}
	dst[rdstsize] = '\0';
	return (rsrcsize);
}
