/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer_handler.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/29 22:01:56 by samzur            #+#    #+#             */
/*   Updated: 2020/09/29 22:53:40 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf.h"

int	handle_int(va_list *args, t_param *param)
{
	char	*str;
	int		nbr;

	nbr = va_arg(*args, int);
	param->flag = (param->flag == 1 && param->prec >= 0) ? 0 : param->flag;
	if (!(str = ft_itoa(nbr)))
		return (-1);
	if (str[0] == '0' && param->prec == 0)
	{
		free(str);
		str = ft_strdup("");
	}
	if (!(handle_int_prec(&str, param))
		|| !(handle_int_flag(&str, param)))
	{
		free(str);
		return (-1);
	}
	return (writer(str, ft_strlen(str)));
}
