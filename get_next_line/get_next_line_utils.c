/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/15 02:53:32 by samzur            #+#    #+#             */
/*   Updated: 2020/09/20 23:51:17 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		ft_strlen(const char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strappend(char *dest, char *src)
{
	void	*cpy;
	int		len_dest;

	len_dest = ft_strlen(dest);
	if ((cpy = (char *)malloc(sizeof(char) * (len_dest + 1))) == NULL)
		return (NULL);
	ft_strcpy(cpy, dest);
	free(dest);
	dest = (char *)malloc(sizeof(char) * (len_dest + ft_strlen(src) + 1));
	if (dest == NULL)
		return (NULL);
	ft_strcpy(dest, cpy);
	free(cpy);
	ft_strcpy(dest + len_dest, src);
	return (dest);
}

char	*ft_strncpy(char *dest, const char *src, int n)
{
	int i;

	i = 0;
	while (src[i] && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	while (i < n)
		dest[i++] = '\0';
	return (dest);
}

char	*ft_strcpy(char *dest, const char *src)
{
	int i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_strdup(const char *str)
{
	int		i;
	int		len;
	char	*cpy;

	len = ft_strlen((char*)str);
	if ((cpy = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	i = 0;
	while (i < len)
	{
		cpy[i] = str[i];
		i++;
	}
	cpy[i] = '\0';
	return (cpy);
}
