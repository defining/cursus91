/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/22 22:23:04 by samzur            #+#    #+#             */
/*   Updated: 2020/09/22 22:23:10 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		free_all(char **ptr, char **left, int ret)
{
	if (ptr != NULL)
	{
		free(*ptr);
		*ptr = NULL;
	}
	if (left != NULL)
	{
		free(*left);
		*left = NULL;
	}
	return (ret);
}

int		if_newline(char *str)
{
	int i;

	i = -1;
	while (str[++i])
		if (str[i] == '\n')
			return (i);
	return (-1);
}

int		read_line(int fd, char **line, char *left)
{
	char	*buf;
	int		ret;
	int		split_pos;

	if ((buf = malloc(sizeof(char) * (BUFFER_SIZE + 1))) == NULL)
		return (free_all(line, NULL, STATUS_ERROR));
	while ((ret = read(fd, buf, BUFFER_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if ((split_pos = if_newline(buf)) != -1)
		{
			ft_strcpy(left, buf + split_pos + 1);
			buf[split_pos] = '\0';
			if ((*line = ft_strappend(*line, buf)) == NULL)
				return (free_all(&buf, NULL, STATUS_ERROR));
			return (free_all(&buf, NULL, STATUS_LINE));
		}
		if ((*line = ft_strappend(*line, buf)) == NULL)
			return (free_all(&buf, NULL, STATUS_ERROR));
	}
	if (ret == -1)
		return (free_all(&buf, line, STATUS_ERROR));
	return (free_all(&buf, NULL, ret));
}

int		get_next_line(int fd, char **line)
{
	int			split_pos;
	static char	left[OPEN_MAX][BUFFER_SIZE + 1] = {{0}};

	if (fd < 0 || fd > OPEN_MAX || line == NULL || BUFFER_SIZE <= 0)
		return (STATUS_ERROR);
	if ((*line = ft_strdup("")) == NULL)
		return (STATUS_ERROR);
	if (left[fd][0] == '\0')
		return (read_line(fd, line, left[fd]));
	if ((split_pos = if_newline(left[fd])) != -1)
	{
		free(*line);
		if ((*line = (char*)malloc(sizeof(char) * (split_pos + 1))) == NULL)
			return (STATUS_ERROR);
		ft_strncpy(*line, left[fd], split_pos);
		(*line)[split_pos] = '\0';
		ft_strcpy(left[fd], left[fd] + split_pos + 1);
		return (STATUS_LINE);
	}
	free(*line);
	if (!(*line = (char*)malloc(sizeof(char) * (ft_strlen(left[fd]) + 1))))
		return (STATUS_ERROR);
	ft_strcpy(*line, left[fd]);
	left[fd][0] = '\0';
	return (read_line(fd, line, left[fd]));
}
