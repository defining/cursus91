/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/15 02:53:44 by samzur            #+#    #+#             */
/*   Updated: 2020/09/20 23:18:59 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <limits.h>
# include <unistd.h>
# include <stdlib.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 32
# endif

# define STATUS_LINE 1
# define STATUS_ERROR -1

/*
** get_next_line.c
*/

int		get_next_line(int fd, char **line);
int		read_line(int fd, char **line, char *left);
int		if_newline(char *str);
int		free_all(char **ptr, char **left, int ret);

/*
** get_next_line_utils.c
*/

char	*ft_strappend(char *dest, char *src);
char	*ft_strncpy(char *dest, const char *src, int n);
int		ft_strlen(const char *str);
char	*ft_strcpy(char *dest, const char *src);
char	*ft_strdup(const char *str);

#endif
