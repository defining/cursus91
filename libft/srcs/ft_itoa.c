/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 01:41:51 by samzur            #+#    #+#             */
/*   Updated: 2020/07/09 01:57:48 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	long long	nbr;
	char		*dst;
	int			i;
	int			neg;

	nbr = n;
	neg = (nbr < 0) ? 1 : 0;
	nbr = (nbr < 0) ? -nbr : nbr;
	i = ft_length_number(nbr);
	if (!(dst = malloc(sizeof(char) * (i + neg + 1))))
		return (0);
	i = 0;
	while (nbr >= 0)
	{
		dst[i++] = nbr % 10 + '0';
		nbr /= 10;
		if (nbr == 0)
			break ;
	}
	if (neg)
		dst[i++] = '-';
	dst[i] = '\0';
	dst = ft_reverse_str(dst);
	return (dst);
}
