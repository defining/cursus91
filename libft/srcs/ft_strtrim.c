/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/08 00:55:22 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 01:02:03 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_detectchar(char c, char const *set)
{
	while (*set)
		if (*set == c)
			return (1);
		else
			set++;
	return (0);
}

char		*ft_strtrim(char const *s1, char const *set)
{
	int		length;
	int		i;
	int		a;
	char	*dst;

	a = 0;
	length = 0;
	if (s1 == NULL || set == NULL)
		return (NULL);
	while (s1[length])
		length++;
	while (ft_detectchar(s1[a], set) && s1[a])
		a++;
	length--;
	while (ft_detectchar(s1[length], set) && length > a)
		length--;
	if (!(dst = malloc(sizeof(char) * (length - a + 2))))
		return (0);
	i = 0;
	while (a <= length && s1[a])
		dst[i++] = s1[a++];
	dst[i] = '\0';
	return (dst);
}
