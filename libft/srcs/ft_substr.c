/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 14:54:57 by samzur            #+#    #+#             */
/*   Updated: 2020/07/09 03:59:46 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(const char *s, unsigned int start, size_t len)
{
	char			*dst;
	unsigned int	i;

	i = 0;
	if (s == NULL)
		return (0);
	if (!(dst = malloc(sizeof(*dst) * (len + 1))))
		return (0);
	while (s[i])
		i++;
	if (i < start)
	{
		*dst = '\0';
		return (dst);
	}
	i = 0;
	s = s + start;
	while (s[i] && len-- > 0)
	{
		dst[i] = s[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}
