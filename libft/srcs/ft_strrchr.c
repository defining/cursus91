/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 02:40:51 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 03:15:22 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *src;
	char *result;

	src = (char *)s;
	result = 0;
	while (*src)
	{
		if (*src == c)
			result = src;
		src++;
	}
	if (*src == c)
		result = src;
	return (result);
}
