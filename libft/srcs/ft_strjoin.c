/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/08 00:25:22 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 00:42:41 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	ft_length(char const *s1, char const *s2)
{
	int	i;
	int	nbr;

	i = 0;
	nbr = 0;
	while (s1[i++])
		nbr++;
	i = 0;
	while (s2[i++])
		nbr++;
	return (nbr);
}

char		*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	char	*dst;
	int		b;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	b = ft_length(s1, s2);
	if (!(dst = malloc(sizeof(char) * (b + 1))))
		return (0);
	i = 0;
	b = 0;
	while (s1[b])
	{
		dst[i] = s1[b++];
		i++;
	}
	b = 0;
	while (s2[b])
	{
		dst[i] = s2[b++];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}
