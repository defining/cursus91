/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 17:37:33 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 18:02:54 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	offset;
	char	*src;

	src = (char *)haystack;
	if (*needle == '\0')
		return (src);
	offset = 0;
	while (offset < len && src[offset])
	{
		i = 0;
		while (src[offset + i] == needle[i] && (offset + i) < len)
		{
			if (needle[i + 1] == '\0')
				return (&src[offset]);
			i++;
		}
		offset++;
	}
	return (NULL);
}
