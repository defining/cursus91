/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/09 22:20:51 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 00:54:46 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *begin;
	t_list *new;
	t_list *tmp;

	if (lst == NULL || (*f) == NULL)
		return (NULL);
	if (!(new = ft_lstnew((*f)(lst->content))))
		return (0);
	lst = lst->next;
	begin = new;
	tmp = new;
	while (lst)
	{
		if (!(new = ft_lstnew((*f)(lst->content))))
		{
			ft_lstclear(&begin, (*del));
			return (0);
		}
		tmp->next = new;
		tmp = new;
		lst = lst->next;
	}
	return (begin);
}
