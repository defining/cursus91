/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 13:57:56 by samzur            #+#    #+#             */
/*   Updated: 2020/07/07 14:03:18 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void *p;
	char *tmp;

	if (!(p = malloc(count * size)))
		return (0);
	tmp = (char *)p;
	count = count * size;
	while (count-- > 0)
	{
		*tmp = '\0';
		tmp++;
	}
	return (p);
}
