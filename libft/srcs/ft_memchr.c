/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:39:15 by samzur            #+#    #+#             */
/*   Updated: 2020/07/06 21:43:18 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*src;
	unsigned char	charact;
	int				i;

	i = 0;
	src = (unsigned char *)s;
	charact = (unsigned char)c;
	while (n-- > 0)
		if (src[i] == charact)
			return (src + i);
		else
			i++;
	return (NULL);
}
