/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_length_number_bonus.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: samzur <samzur@student.s19.be>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 01:56:18 by samzur            #+#    #+#             */
/*   Updated: 2020/07/15 01:56:22 by samzur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_length_number(long long nbr)
{
	int	i;

	i = 0;
	while (nbr >= 0)
	{
		nbr /= 10;
		i++;
		if (nbr == 0)
			break ;
	}
	return (i);
}
