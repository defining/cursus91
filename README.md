# CURSUS 91 - General informations

My cursus into 91 program. 

Kick-off event: 1st July 2020
First commit: 3rd July 2020

# 001 - Libft - Started 3 July 2020

Basic Functions (22):

- ~~memset~~
- ~~bzero~~
- ~~memcpy~~
- ~~memccpy~~
- ~~memmove~~
- ~~memchr~~
- ~~memcmp~~ 
- ~~strlen~~
- ~~isalpha~~
- ~~isdigit~~
- ~~isalnum~~
- ~~isascii~~
- ~~isprint~~
- ~~toupper~~
- ~~tolower~~
- ~~strchr~~
- ~~strrchr~~
- ~~strncmp~~
- ~~strlcpy~~
- ~~strlcat~~
- ~~strnstr~~
- ~~atoi~~

With Malloc (2):

- ~~calloc~~
- ~~strdup~~

Additional functions (10):

- ~~ft_substr~~
- ~~ft_strjoin~~
- ~~ft_strtrim~~
- ~~ft_split~~
- ~~ft_itoa~~
- ~~ft_strmapi~~
- ~~ft_putchar_fd~~
- ~~ft_putstr_fd~~
- ~~ft_putendl_fd~~
- ~~ft_putnbr_fd~~

Bonus functions (9):

- ~~ft_lstnew~~
- ~~ft_lstadd_front~~
- ~~ft_lstsize~~
- ~~ft_lstlast~~
- ~~ft_lstadd_back~~
- ~~ft_lstdelone~~
- ~~ft_lstclear~~
- ~~ft_lstiter~~
- ~~ft_lstmap~~

Utils

- ~~ft_putchar~~
- ~~ft_putstr~~
- ~~ft_free_tab_str~~
- ~~ft_count_split~~
- ~~ft_length_number~~
- ~~ft_reverse_str~~
